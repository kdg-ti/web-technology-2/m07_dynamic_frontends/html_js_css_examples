
// Klassieke for loop WT1
// let list = document.querySelectorAll("h2, p.wel");
// for (let i = 0; i < list.length; i++) {
//   list[i].style.color = "red";
// }

// forEach
// document.querySelectorAll("h2, p.wel")
//   .forEach(p => p.style.color = "red");

// for..of when using getElementsByTagName
// for (let p of document.getElementsByTagName("p")){
//   p.style.color = "red"
// }

// spread operator  when using getElementsByTagName
[...document.getElementsByTagName("p")]
  .forEach(p => p.style.color = "red");

